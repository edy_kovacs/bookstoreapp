﻿using System;
using BookStoreApp.iOS.CustomRenderers;
using BookStoreApp.Utils.CustomElements;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRenderer))]
namespace BookStoreApp.iOS.CustomRenderers
{
    public class CustomLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var label = Element as CustomLabel;
                Control.Lines = label.LinesNumber;
            }
        }
    }
}
