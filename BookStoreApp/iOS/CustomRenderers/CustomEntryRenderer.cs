﻿using System;
using BookStoreApp.iOS.CustomRenderers;
using BookStoreApp.Utils.CustomElements;
using BookStoreApp.Utils.Enums;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace BookStoreApp.iOS.CustomRenderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var entry = Element as CustomEntry;
                if(entry.HasBorder == false)
                {
                    Control.Layer.BorderWidth = 0;
                    Control.BorderStyle = UITextBorderStyle.None;
                }

                SetReturnType(entry);
                Control.ShouldReturn += (UITextField tf) => { return InvokeCompleteFunction(entry); };
            }
            else if (e.OldElement != null)
            {
                var entry = Element as CustomEntry;
                Control.ShouldReturn -= (UITextField tf) => { return InvokeCompleteFunction(entry); };
            }
        }

        private void SetReturnType(CustomEntry entry)
        {
            ReturnType type = entry.ReturnType;

            switch (type)
            {
                case ReturnType.Go:
                    Control.ReturnKeyType = UIReturnKeyType.Go;
                    break;
                case ReturnType.Next:
                    Control.ReturnKeyType = UIReturnKeyType.Next;
                    break;
                case ReturnType.Send:
                    Control.ReturnKeyType = UIReturnKeyType.Send;
                    break;
                case ReturnType.Search:
                    Control.ReturnKeyType = UIReturnKeyType.Search;
                    break;
                case ReturnType.Done:
                    Control.ReturnKeyType = UIReturnKeyType.Done;
                    break;
                default:
                    Control.ReturnKeyType = UIReturnKeyType.Default;
                    break;
            }
        }

        public bool InvokeCompleteFunction(CustomEntry entry)
        {
            entry.InvokeCompleted();
            return true;
        }
    }
}
