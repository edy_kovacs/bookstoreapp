﻿using System;
using BookStoreApp.Models;
using BookStoreApp.Utils.Exceptions;

namespace BookStoreApp.Validators
{
    public static class BookValidator
    {
        public static void Validate(Book entity)
        {
            var errorMessage = "";

            if (string.IsNullOrWhiteSpace(entity.Title))
            {
                errorMessage += "Title field cannot be empty... \n";
            }

            if (string.IsNullOrWhiteSpace(entity.Author))
            {
                errorMessage += "Author field cannot be empty... \n";
            }

            if (string.IsNullOrWhiteSpace(entity.Genre))
            {
                errorMessage += "Genre field cannot be empty... \n";
            }

            if (entity is ConferencePaper conferencePaper)
            {
                if (string.IsNullOrWhiteSpace(conferencePaper.FirstConferenceName))
                {
                    errorMessage += "The name of the conference where it was first presented cannot be void... \n";
                }

                if (string.IsNullOrWhiteSpace(conferencePaper.Abstract))
                {
                    errorMessage += "Every Conference Paper should have a short description... \n";
                }
            }

            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                throw new ValidationException(errorMessage);
            }
        }
    }
}
