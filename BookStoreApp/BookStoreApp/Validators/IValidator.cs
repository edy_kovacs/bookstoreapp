﻿using System;
namespace BookStoreApp.Validators
{
    // Interface to be implemented by the validators.
    // T - Type of entity to be validated
    public interface IValidator<T>
    {
        void Validate(T entity);
    }
}
