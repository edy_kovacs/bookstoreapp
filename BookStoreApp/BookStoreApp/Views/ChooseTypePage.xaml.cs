﻿using System;
using System.Collections.Generic;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Utils.Converters;
using BookStoreApp.ViewModels;
using Xamarin.Forms;

namespace BookStoreApp.Views
{
    public partial class ChooseTypePage : ContentPage
    {
        private Frame _lastFrameChosen;

        public ChooseTypePage(ChooseTypeViewModel viewModel)
        {
            InitializeComponent();

            ViewModel = viewModel;

            nextButton.IsEnabled = false;
            nextButton.Opacity = 0;

            NavigationPage.SetBackButtonTitle(this, String.Empty);
        }

        public ChooseTypeViewModel ViewModel
        {
            get { return BindingContext as ChooseTypeViewModel; }
            set { BindingContext = value; }
        }

        void TypeTapped(object sender, System.EventArgs e)
        {
            var typeClicked = (Frame)sender;
            StackLayout stackLayoutInside;
            Image imageInside;
            Label labelInside;


            if(_lastFrameChosen != null)
            {
                stackLayoutInside = (StackLayout)_lastFrameChosen.Content;
                imageInside = (Image)stackLayoutInside.Children[0];
                labelInside = (Label)stackLayoutInside.Children[1];

                _lastFrameChosen.BackgroundColor = Color.White;
                labelInside.TextColor = CustomColors.Gray40;
                imageInside.Source = (ImageSource)ImageStringConverter.GetSimpleImageString(((FileImageSource)imageInside.Source).File);
            }

            _lastFrameChosen = typeClicked;
            stackLayoutInside = (StackLayout)_lastFrameChosen.Content;
            imageInside = (Image)stackLayoutInside.Children[0];
            labelInside = (Label)stackLayoutInside.Children[1];

            _lastFrameChosen.BackgroundColor = CustomColors.ThemeBlue;
            imageInside.Source = (ImageSource)ImageStringConverter.GetActiveImageString(((FileImageSource)imageInside.Source).File);
            labelInside.TextColor = Color.White;

            nextButton.IsEnabled = true;
            nextButton.Opacity = 1;
        }
    }
}
