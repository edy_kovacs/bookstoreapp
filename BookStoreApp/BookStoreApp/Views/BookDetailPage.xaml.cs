﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BookStoreApp.Models;
using BookStoreApp.Utils.Converters;
using BookStoreApp.ViewModels;
using Xamarin.Forms;

namespace BookStoreApp.Views
{
    public partial class BookDetailPage : ContentPage
    {
        public BookDetailPage(BookDetailViewModel viewModel)
        {
            InitializeComponent();

            ViewModel = viewModel;

            if(String.IsNullOrEmpty(ViewModel.ViewModel.Title))
            {
                this.Title = ViewModelToAddStringConverter.Convert(viewModel.ViewModel);
            }
        }

        public BookDetailViewModel ViewModel
        {
            get { return BindingContext as BookDetailViewModel; }
            set { BindingContext = value; }
        }

        void SelectedConferencePaperChanged(object sender, System.EventArgs e)
        {
            if (sender == null)
            {
                return;
            }

            var picker = sender as Picker;

            if (picker.SelectedItem != null)
            {
                var conferencePaper = picker.SelectedItem as ConferencePaper;
                ViewModel.SelectedConferencePaperCommand.Execute(conferencePaper);
            }
        }

        void SelectedRecurrenceChanged(object sender, System.EventArgs e)
        {
            if(sender == null)
            {
                return;
            }

            var picker = sender as Picker;

            if (picker.SelectedItem != null)
            {
                var recurrence = (Recurrence)Enum.Parse(typeof(Recurrence), picker.SelectedItem.ToString());
                ViewModel.SelectedRecurrenceCommand.Execute(recurrence);
            }
        }

        void AbstractChanged(object sender, System.EventArgs e)
        {
            var entry = sender as Entry;
            ViewModel.AbstractChangedCommand.Execute(entry.Text);
        }

        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            var listview = sender as ListView;
            listview.SelectedItem = null;
        }

        void ReferenceAdded(object sender, System.EventArgs e)
        {
            var entry = sender as Entry;
            ViewModel.AddReferenceCommand.Execute(entry.Text);
            entry.Text = "";
        }

        void ReferenceDeleted(object sender, System.EventArgs e)
        {
            ViewModel.DeleteReferenceCommand.Execute((sender as MenuItem).CommandParameter);
        }

        void ContainedConferencePaperDeleted(object sender, System.EventArgs e)
        {
            ViewModel.DeleteContainedConferencePaperCommand.Execute((sender as MenuItem).CommandParameter);
        }

        void OnBookFocus(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            ViewModel.WasBookModifiedCommand.Execute(null);
        }

        void TitleCompleted(object sender, System.EventArgs e)
        {
            authorEntry.Focus();
        }

        void AuthorCompleted(object sender, System.EventArgs e)
        {
            genreEntry.Focus();
        }

        void GenreCompleted(object sender, System.EventArgs e)
        {
            dateOfPublishPicker.Focus();
        }

        void DateOfPublishCompleted(object sender, System.EventArgs e)
        {
            if(ViewModel.ViewModel is JournalViewModel)
            {
                firstDateOfPublishPicker.Focus();
            }
            else if(ViewModel.ViewModel is ConferencePaperViewModel)
            {
                firstConferenceNameEntry.Focus();
            }
            else
            {
                ViewModel.SaveCommand.Execute(null);   
            }
        }

        void FirstDateOfPublishCompleted(object sender, System.EventArgs e)
        {
            recurrencePicker.Focus();
        }

        void FirstConferenceNameCompleted(object sender, System.EventArgs e)
        {
            conferenceLocationEntry.Focus();
        }

        void ConferenceLocationCompleted(object sender, System.EventArgs e)
        {
            abstractEntry.Focus();
        }
    }
}
