﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookStoreApp.Models;
using BookStoreApp.Services;
using BookStoreApp.ViewModels;
using Xamarin.Forms;

namespace BookStoreApp.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            ViewModel = new LoginViewModel();

            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();
        }

        public LoginViewModel ViewModel
        {
            get { return BindingContext as LoginViewModel; }
            set { BindingContext = value; }
        }

        void EmailCompleted(object sender, System.EventArgs e)
        {
            passwordEntry.Focus();
        }

        void PasswordCompleted(object sender, System.EventArgs e)
        {
            if(ViewModel.WasRegisterClicked)
            {
                confirmEntry.Focus();
            }
            else
            {
                ViewModel.LoginButtonCommand.Execute(null);
            }
        }

        void ConfirmCompleted(object sender, System.EventArgs e)
        {
            ViewModel.RegisterButtonCommand.Execute(null);
        }
    }
}
