﻿using System;
using System.Collections.Generic;
using BookStoreApp.ViewModels;
using Xamarin.Forms;

namespace BookStoreApp.Views
{
    public partial class MenuPage : ContentPage
    {
        public MenuPage(SideMenuViewModel sideMenuViewModel)
        {
            ViewModel = sideMenuViewModel;

            InitializeComponent();
        }

        public SideMenuViewModel ViewModel
        {
            get { return BindingContext as SideMenuViewModel; }
            set { BindingContext = value; }
        }
    }
}
