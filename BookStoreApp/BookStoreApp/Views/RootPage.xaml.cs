﻿using BookStoreApp.Utils.CustomElements;
using Xamarin.Forms;

namespace BookStoreApp.Views
{
    public partial class RootPage : CustomMasterDetailPage
    {
        public RootPage()
        {
            InitializeComponent();

            MasterBehavior = MasterBehavior.Popover;

            if(Device.RuntimePlatform == Device.iOS)
            {
                IsGestureEnabled = false;
            }
        }
    }
}
