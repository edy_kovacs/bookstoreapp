﻿using System;
using System.Collections.Generic;
using BookStoreApp.Services;
using BookStoreApp.ViewModels;
using Xamarin.Forms;

namespace BookStoreApp.Views
{
    public partial class BooksListPage : ContentPage
    {
        private bool _wasLoadedOnce = false;

        public BooksListPage()
        {
            NavigationPage.SetBackButtonTitle(this, String.Empty);

            ViewModel = new BooksViewModel();

            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (_wasLoadedOnce == false)
            {
                ViewModel.LoadDataCommand.Execute(null);
                _wasLoadedOnce = true;
            }

            base.OnAppearing();
        }

        void OnBookDelete(object sender, System.EventArgs e)
        {
            ViewModel.DeleteBookCommand.Execute((sender as MenuItem).CommandParameter);
        }

        public BooksViewModel ViewModel
        {
            get { return BindingContext as BooksViewModel; }
            set { BindingContext = value; }
        }

        void OnBookSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ViewModel.SelectBookCommand.Execute(e.SelectedItem);
        }
    }
}
