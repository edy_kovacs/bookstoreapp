﻿using System;
using System.Collections.Generic;
using BookStoreApp.ViewModels;
using Xamarin.Forms;

namespace BookStoreApp.Views
{
    public partial class PreviewBookPage : ContentPage
    {
        public PreviewBookPage(PreviewBookViewModel viewModel)
        {
            InitializeComponent();

            ViewModel = viewModel;

            NavigationPage.SetBackButtonTitle(this, String.Empty);
        }

        public PreviewBookViewModel ViewModel
        {
            get { return BindingContext as PreviewBookViewModel; }
            set { BindingContext = value; }
        }
    }
}
