﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using BookStoreApp.Utils.Constants;

namespace BookStoreApp.Utils.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<HttpResponseMessage> PatchAsync(this HttpClient client, Uri requestUri, HttpContent content)
        {
            var method = new HttpMethod(HttpVerbNames.Patch);
            var request = new HttpRequestMessage(method, requestUri)
            {
                Content = content
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (TaskCanceledException e)
            {
                Debug.WriteLine(e.ToString());
            }

            return response;
        }
    }
}
