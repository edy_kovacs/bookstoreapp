﻿using System;
using BookStoreApp.Utils.Constants;

namespace BookStoreApp.Utils.Factories
{
    public static class ImageUriFactory
    {
        public static string GetUri()
        {
            var rnd = new Random();
            return Messages.ImageBaseURL + rnd.Next(780, 790).ToString();
        }
    }
}
