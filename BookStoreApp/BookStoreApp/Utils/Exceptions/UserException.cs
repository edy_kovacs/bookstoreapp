﻿using System;

namespace BookStoreApp.Utils.Exceptions
{
    public class UserException : Exception
    {
        public UserException(string message) : base(message) { }
    }
}
