﻿using System;
namespace BookStoreApp.Utils.Exceptions
{
    public class RepositoryException : UserException
    {
        public RepositoryException(string message) : base(message) { }
    }
}
