﻿using System;

namespace BookStoreApp.Utils.Exceptions
{
    // Validation Exception thrown by the validation methods
    public class ValidationException : UserException
    {
        public ValidationException(string message) : base(message) { }
    }
}
