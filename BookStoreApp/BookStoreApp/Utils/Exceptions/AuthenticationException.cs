﻿using System;
namespace BookStoreApp.Utils.Exceptions
{
    public class AuthenticationException : UserException
    {
        public AuthenticationException(string message) : base(message) { }
    }
}
