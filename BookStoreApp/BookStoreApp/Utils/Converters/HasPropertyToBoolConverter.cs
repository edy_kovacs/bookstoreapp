﻿using System;
using System.Reflection;
using System.Diagnostics;
using System.Globalization;
using BookStoreApp.ViewModels;
using Xamarin.Forms;

namespace BookStoreApp.Utils.Converters
{
    /// Returns true if the string contained in "parameter" variable is a property of the object received into the "value" variable
    public class HasPropertyToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.GetType().GetRuntimeProperty(parameter.ToString()) != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.GetType().GetRuntimeProperty(parameter.ToString()) == null;
        }
    }
}
