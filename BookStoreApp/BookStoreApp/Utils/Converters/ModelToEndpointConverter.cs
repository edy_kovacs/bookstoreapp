﻿using System;
using BookStoreApp.Models;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Utils.Extensions;

namespace BookStoreApp.Utils.Converters
{
    public static class ModelToEndpointConverter
    {
        public static Uri Convert(Book book)
        {
            string stringToReturn = String.Empty;

            TypeSwitch.Switch(
                book,
                TypeSwitch.Case<Journal>(() => stringToReturn = RESTStrings.AddJournalURL),
                TypeSwitch.Case<ConferencePaper>(() => stringToReturn = RESTStrings.AddConferencePaperURL),
                TypeSwitch.Default(() => stringToReturn = RESTStrings.AddBookURL));

            return new Uri(stringToReturn);
        }

        public static Uri ConvertToDeleteEndpoint(Book book)
        {
            string stringToReturn = String.Empty;

            TypeSwitch.Switch(
                book,
                TypeSwitch.Case<Journal>(() => stringToReturn = RESTStrings.DeleteJournalURL + book.Id),
                TypeSwitch.Case<ConferencePaper>(() => stringToReturn = RESTStrings.DeleteConferencePaperURL + book.Id),
                TypeSwitch.Default(() => stringToReturn = RESTStrings.DeleteBookURL + book.Id));

            return new Uri(stringToReturn);
        }
    }
}
