﻿using System;
using BookStoreApp.Utils.Constants;

namespace BookStoreApp.Utils.Converters
{
    public static class ImageStringConverter
    {
        public static string GetActiveImageString(string filename)
        {
            return filename.Split(Messages.DotChar)[0] + Messages.UnderscoreActiveString;
        }

        public static string GetSimpleImageString(string filename)
        {
            return filename.Split(Messages.UnderscoreChar)[0] + Messages.DotPngString;
        }
    }
}
