﻿using System;
using System.Diagnostics;
using BookStoreApp.Models;
using BookStoreApp.ViewModels;
using Newtonsoft.Json;
using System.Linq;

namespace BookStoreApp.Utils.Converters
{
    public static class ViewModelToDomainModelConverter
    {

        #region ToDomainModel Methods

        public static Book ToDomainModel(BookViewModel bookViewModel)
        {
            if (bookViewModel is ConferencePaperViewModel conferencePaperViewModel)
            {
                return new ConferencePaper()
                {
                    Id = conferencePaperViewModel.Id,
                    Title = conferencePaperViewModel.Title,
                    Author = conferencePaperViewModel.Author,
                    Genre = conferencePaperViewModel.Genre,
                    PublishedDate = conferencePaperViewModel.PublishedDate,
                    ImageURI = conferencePaperViewModel.ImageURI,
                    FirstConferenceName = conferencePaperViewModel.FirstConferenceName,
                    ConferenceLocation = conferencePaperViewModel.ConferenceLocation,
                    Abstract = conferencePaperViewModel.Abstract,
                    References = conferencePaperViewModel.References.ToList()
                };
            }
            else if (bookViewModel is JournalViewModel journalViewModel)
            {
                return new Journal()
                {
                    Id = journalViewModel.Id,
                    Title = journalViewModel.Title,
                    Author = journalViewModel.Author,
                    Genre = journalViewModel.Genre,
                    PublishedDate = journalViewModel.PublishedDate,
                    ImageURI = journalViewModel.ImageURI,
                    FirstPublished = journalViewModel.FirstPublished,
                    Recurrence = journalViewModel.Recurrence,
                    ConferencePapers = journalViewModel.conferencePapers
                };
            }
            else
            {
                return new Book()
                {
                    Id = bookViewModel.Id,
                    Title = bookViewModel.Title,
                    Author = bookViewModel.Author,
                    Genre = bookViewModel.Genre,
                    PublishedDate = bookViewModel.PublishedDate,
                    ImageURI = bookViewModel.ImageURI
                };
            }

        }

        #endregion

        #region ToViewModel Methods

        public static BookViewModel ToViewModel(Book book)
        {
            if (book is Journal journal)
            {
                return new JournalViewModel(journal);
            }
            else if (book is ConferencePaper conferencePaper)
            {
                return new ConferencePaperViewModel(conferencePaper);
            }
            else
            {
                return new BookViewModel(book);
            }
        }

        #endregion

        #region CloneViewModel methods

        public static BookViewModel CloneViewModel(BookViewModel VMToBeCloned)
        {
            if (VMToBeCloned is JournalViewModel journalViewModel)
            {
                return Clone<JournalViewModel>(journalViewModel);
            }
            else if (VMToBeCloned is ConferencePaperViewModel conferencePaperViewModel)
            {
                return Clone<ConferencePaperViewModel>(conferencePaperViewModel);
            }
            else
            {
                return Clone<BookViewModel>(VMToBeCloned);
            }
        }

        public static T Clone<T>(this T source)
        {
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source));
        }

        #endregion

    }
}
