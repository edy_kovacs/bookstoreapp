﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using BookStoreApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BookStoreApp.Utils.Converters
{
    public class IntToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)value == 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !((int)value == 0);
        }
    }
}
