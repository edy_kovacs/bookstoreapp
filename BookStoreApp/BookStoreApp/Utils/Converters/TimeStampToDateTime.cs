﻿using System;
namespace BookStoreApp.Utils.Converters
{
    public static class TimeStampToDateTime
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static double DateTimeToUnixTimeStamp(DateTime value)
        {
            double epoch = (value.Ticks - 621355968000000000) / 10000000;
            return epoch;
        }
    }
}
