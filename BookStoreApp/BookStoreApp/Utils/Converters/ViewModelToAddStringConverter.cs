﻿using System;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Utils.Extensions;
using BookStoreApp.ViewModels;

namespace BookStoreApp.Utils.Converters
{
    public static class ViewModelToAddStringConverter
    {
        public static string Convert(BookViewModel viewModel)
        {
            string addANewStringBase = Messages.AddANewStringBase;
            string stringToReturn = String.Empty;

            TypeSwitch.Switch(
                viewModel,
                TypeSwitch.Case<JournalViewModel>(() =>
                {
                    string journalString = Messages.JournalString;
                    stringToReturn = $"{addANewStringBase} {journalString}";
                }),
                TypeSwitch.Case<ConferencePaperViewModel>(() =>
                {
                    string conferencePaperString = Messages.PaperString;
                    stringToReturn = $"{addANewStringBase} {conferencePaperString}";
                }),
                TypeSwitch.Default(() =>
                {
                    string bookString = Messages.BookString;
                    stringToReturn = $"{addANewStringBase} {bookString}";
                }));

            return stringToReturn;
        }
    }
}
