﻿using System;

namespace BookStoreApp.Utils.Enums
{
    public enum ReturnType
    {
        Go,
        Next,
        Done,
        Send,
        Search
    }
}
