﻿using System;
using BookStoreApp.Utils.Enums;
using Xamarin.Forms;

namespace BookStoreApp.Utils.CustomElements
{
    public class CustomEntry : Entry
    {
        public new event EventHandler Completed;

        public static readonly BindableProperty HasBorderProperty = BindableProperty.Create(
            nameof(HasBorder),
            typeof(bool),
            typeof(CustomEntry),
            true,
            BindingMode.OneWay);

        public static readonly BindableProperty ReturnTypeProperty = BindableProperty.Create(
            nameof(ReturnType),
            typeof(ReturnType),
            typeof(CustomEntry),
            ReturnType.Done,
            BindingMode.OneWay);

        public bool HasBorder
        {
            get { return (bool)GetValue(HasBorderProperty); }
            set { SetValue(HasBorderProperty, value); }
        }

        public ReturnType ReturnType
        {
            get { return (ReturnType)GetValue(ReturnTypeProperty); }
            set { SetValue(ReturnTypeProperty, value); }
        }

        public void InvokeCompleted()
        {
            if (this.Completed != null)
            {
                this.Completed.Invoke(this, null);
            }
        }
    }
}
