﻿using System;
using Xamarin.Forms;

namespace BookStoreApp.Utils.CustomElements
{
    public class CustomLabel : Label
    {
        public static readonly BindableProperty LinesNumberProperty = BindableProperty.Create(
            nameof(LinesNumber),
            typeof(int),
            typeof(CustomLabel),
            3,
            BindingMode.OneWay);

        public int LinesNumber
        {
            get { return (int)GetValue(LinesNumberProperty); }
            set { SetValue(LinesNumberProperty, value); }
        }
    }
}
