﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BookStoreApp.Utils.Constants
{
    public static class RESTSettings
    {
        public static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };
    }
}
