﻿using System;

namespace BookStoreApp.Utils.Constants
{
    public static class Messages
    {

        #region User Messages

        public static readonly string PleaseFixMessage = "Please fix the following problems and try again";
        public static readonly string ErrorMessage = "Error";
        public static readonly string WarningMessage = "Warning";
        public static readonly string CannotAddConferencePaper = "The selected Conference Paper is already contained in this Journal";
        public static readonly string DeleteConfirmation = "Are you sure you want to delete the selected book?";
        public static readonly string DeleteConfirmationDesc = "By doing this you will lose the book.";
        public static readonly string IncorrectCompletionOfFields = "Please make sure you have comepleted all the fields correctly before trying to login";
        public static readonly string InvalidCredentials = "Invalid Credentials";
        public static readonly string AuthTokenNotGranted = "Unfortunately, we could not provide an authentication token for this device. Please, try again later...";
        public static readonly string WeAreSorry = "We're Sorry...";
        public static readonly string UnexpectedError = "An unexpected error ocurred while trying to log you out. Please, try again...";
        public static readonly string PasswordTooShort = "The password you chosen is too short. Please choose another password...";
        public static readonly string PasswordsDontMatch = "The passwords don't match. Please make sure you enter the same password in both fields...";
        public static readonly string DuplicateEmail = "An account associated with this email already exists. Please choose a different one...";
        public static readonly string SuccesfullyRegistered = "Your account has been registered succesfully!";
        public static readonly string Success = "Succes";
        public static readonly string BackToLogin = "Back to Login";
        public static readonly string ErrorLoadingBooks = "Something went wrong! We couldn't retrieve your list of books. Please try again later...";
        public static readonly string DeleteNotAllowed = "Delete not allowed once you logged out...";
        public static readonly string ErrorAddingBook = "Something went wrong! We couldn't add the book in our database. It is totally our fault. Please try again later...";
        public static readonly string ErrorDeletingBook = "Something went wrong! We couldn't delete the book from our database. It is totally our fault. Please try again later...";
        public static readonly string ErrorUpdatingBook = "Something went wrong! We couldn't update the book. It is totally our fault. Please try again later...";
        public static readonly string SessionExpired = "The book was not deleted because you were logged out in the meantime...";
        public static readonly string AlreadyLoggedOut = "You have already been logged out...";
        public static readonly string CheckConnectionString = "Please check your internet conection and try again...";
        public static readonly string ErrorBookAlreadyDeleted = "That book was already deleted by you. It just took some time for us to update the list of books..";
        public static readonly string WhileLoggingOut = "You cannot add a new book while logging out...";

        #endregion

        #region Button Strings

        public static readonly string DeleteMessageString = "Delete";
        public static readonly string CancelMessageString = "Cancel";
        public static readonly string OkMessageString = "Ok";

        #endregion

        #region Event Names Strings

        public static readonly string BookAddedEventString = "BookAdded";
        public static readonly string LogoutStartedEventString = "LogoutProcedureStarted";
        public static readonly string LogoutEndedventString = "LogoutProcedureEnded";

        #endregion

        #region Entities Types Strings

        public static readonly string ConferencePaperString = "ConferencePaper";
        public static readonly string PaperString = "Paper";
        public static readonly string JournalString = "Journal";
        public static readonly string BookString = "Book";
        public static readonly string RecurrenceString = "Recurrence";

        #endregion

        #region Strings for Converters Converter

        public static readonly string DotPngString = ".png";
        public static readonly string UnderscoreActiveString = "_active.png";
        public static readonly char DotChar = '.';
        public static readonly char UnderscoreChar = '_';
        public static readonly string ImageBaseURL = "http://picsum.photos/900/600?image=";
        public static readonly string AddANewStringBase = "Add a new";

        #endregion

    }
}
