﻿using System;
namespace BookStoreApp.Utils.Constants
{
    public static class RESTStrings
    {
        public static readonly string BaseURL = "https://studentplan.tk/bookStoreApp";
        public static readonly string AuthURL = $"{BaseURL}/auth";
        public static readonly string SignOutURL = $"{BaseURL}/signout";
        public static readonly string SignUpURL = $"{BaseURL}/signup";
        public static readonly string AddBookURL = $"{BaseURL}/book";
        public static readonly string AddJournalURL = $"{BaseURL}/journal";
        public static readonly string AddConferencePaperURL = $"{BaseURL}/conferencePaper";
        public static readonly string DeleteBookURL = $"{AddBookURL}/";
        public static readonly string DeleteJournalURL = $"{AddJournalURL}/";
        public static readonly string DeleteConferencePaperURL = $"{AddConferencePaperURL}/";
        public static readonly string PublishablesURL = $"{BaseURL}/publishables";
        public static readonly string JsonTypeOfContent = "application/json";
        public static readonly string XAuthString = "x-auth";
        public static readonly string BooksList = "books";
        public static readonly string ConferencePapersList = "conferencePapers";
        public static readonly string JournalsList = "journals";
        public static readonly string UnauthorizedErrorString = "Unauthorized";
        public static readonly string BadRequestErrorString = "Bad Request";
        public static readonly string EmailString = "email";
    }
}