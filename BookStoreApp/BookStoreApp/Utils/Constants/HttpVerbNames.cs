﻿using System;
namespace BookStoreApp.Utils.Constants
{
    public static class HttpVerbNames
    {
        public static readonly string Patch = "PATCH";
    }
}
