﻿using System;
namespace BookStoreApp.Utils.Constants
{
    public static class NumericConstants
    {
        public readonly static int MinimumPasswordLength = 4;
    }
}
