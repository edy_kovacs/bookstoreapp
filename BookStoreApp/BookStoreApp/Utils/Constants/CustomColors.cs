﻿using System;
using Xamarin.Forms;

namespace BookStoreApp.Utils.Constants
{
    public static class CustomColors
    {
        public static Color ThemeBlue
        {
            get
            {
                return (Color)Application.Current.Resources["ThemeBlue"];
            }
        }

        public static Color Gray40
        {
            get
            {
                return (Color)Application.Current.Resources["Gray40"];
            }
        }

        public static Color Gray60
        {
            get
            {
                return (Color)Application.Current.Resources["Gray60"];
            }
        }

        public static Color Gray90
        {
            get
            {
                return (Color)Application.Current.Resources["Gray90"];
            }
        }

        public static Color WhiteThree
        {
            get
            {
                return (Color)Application.Current.Resources["WhiteThree"];
            }
        }
    }
}