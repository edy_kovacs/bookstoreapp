﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using BookStoreApp.Services;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Utils.Exceptions;
using BookStoreApp.Views;
using Xamarin.Forms;

namespace BookStoreApp.ViewModels
{
    public class SideMenuViewModel : BaseViewModel
    {
        public string Username { get; set; }
        public ICommand LogoutCommand { get; private set; }

        public SideMenuViewModel()
        {
            Username = AuthenticationManager.Instance.Username;

            LogoutCommand = new Command(async () => await LogoutHandlerAsync());
        }

        public async Task LogoutHandlerAsync()
        {
            IsBusy = true;
            MessagingCenter.Send<SideMenuViewModel>(this, Messages.LogoutStartedEventString);
            try
            {
                await AuthenticationService.Instance.LogoutAsync();
                Application.Current.MainPage = new NavigationPage(new LoginPage());
            }
            catch (AuthenticationException exception)
            {
                await PageService.Instance.DisplayAlertAsync(Messages.WeAreSorry, exception.Message, Messages.OkMessageString);
            }
            catch (HttpRequestException)
            {
                await PageService.Instance.DisplayAlertAsync(Messages.PleaseFixMessage, Messages.CheckConnectionString, Messages.OkMessageString);
            }
            MessagingCenter.Send<SideMenuViewModel>(this, Messages.LogoutEndedventString);
            IsBusy = false;
        }
    }
}
