﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using BookStoreApp.Models;
using BookStoreApp.Services;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Views;
using Xamarin.Forms;

namespace BookStoreApp.ViewModels
{
    public class ChooseTypeViewModel : BaseViewModel
    {
        private string ChosenType { get; set; }

        public ICommand NextCommand { get; private set; }
        public ICommand TypeChosenCommand { get; private set; }

        public ChooseTypeViewModel()
        {
            NextCommand = new Command(async () => await OpenAddPageAsync());
            TypeChosenCommand = new Command<string>(type => ChosenTypeChanged(type));
        }

        async Task OpenAddPageAsync()
        {
            IsBusy = true;
            BookViewModel viewModel;

            if (ChosenType == Messages.ConferencePaperString)
            {
                viewModel = new ConferencePaperViewModel();
            }
            else if (ChosenType == Messages.JournalString)
            {
                viewModel = new JournalViewModel();
            }
            else
            {
                viewModel = new BookViewModel();
            }

            var viewModelToPass = new BookDetailViewModel(viewModel);
            await PageService.Instance.PushAsync(new BookDetailPage(viewModelToPass));
            IsBusy = false;
        }

        void ChosenTypeChanged(string type)
        {
            if (ChosenType != type)
            {
                ChosenType = type;
            }
        }
    }
}
