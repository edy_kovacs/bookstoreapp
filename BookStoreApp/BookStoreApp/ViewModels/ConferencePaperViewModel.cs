﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BookStoreApp.Models;

namespace BookStoreApp.ViewModels
{
    public class ConferencePaperViewModel : BookViewModel
    {
        public ConferencePaperViewModel()
        {
            References = new ObservableCollection<string>();
        }

        public ConferencePaperViewModel(ConferencePaper conferencePaper) : base(conferencePaper)
        {
            FirstConferenceName = conferencePaper.FirstConferenceName;
            Abstract = conferencePaper.Abstract;
            ConferenceLocation = conferencePaper.ConferenceLocation;

            if (conferencePaper.References == null)
            {
                References = new ObservableCollection<string>();
            }
            else
            {
                References = new ObservableCollection<string>(conferencePaper.References);
            }

        }

        private string _firstConferenceName;
        public string FirstConferenceName
        {
            get
            {
                return _firstConferenceName;
            }
            set
            {
                SetValue(ref _firstConferenceName, value);
            }
        }

        private ObservableCollection<string> _references = new ObservableCollection<string>();
        public ObservableCollection<string> References
        {
            get
            {
                return _references;
            }
            set
            {
                SetValue(ref _references, value);
            }
        }

        private string _abstract;
        public string Abstract
        {
            get
            {
                return _abstract;
            }
            set
            {
                SetValue(ref _abstract, value);
            }
        }

        private string _conferenceLocation;
        public string ConferenceLocation
        {
            get
            {
                return _conferenceLocation;
            }
            set
            {
                SetValue(ref _conferenceLocation, value);
            }
        }

        public override string ToString()
        {
            return $"{Title} - {DateOfPublish:dd:MMM:yyyy}";
        }
    }
}