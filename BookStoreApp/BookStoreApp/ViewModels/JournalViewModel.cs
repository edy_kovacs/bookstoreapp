﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BookStoreApp.Models;
using BookStoreApp.Services;
using BookStoreApp.Utils.Converters;

namespace BookStoreApp.ViewModels
{
    public class JournalViewModel : BookViewModel
    {
        public JournalViewModel()
        {
        }

        public JournalViewModel(Journal journal) : base(journal)
        {
            FirstPublished = journal.FirstPublished;
            conferencePapers = journal.ConferencePapers;
            Recurrence = journal.Recurrence;
        }

        public List<string> conferencePapers { get; set; } = new List<string>();

        public ObservableCollection<ConferencePaper> _containedConferencePapers = new ObservableCollection<ConferencePaper>();
        public ObservableCollection<ConferencePaper> ContainedConferencePapers
        {
            get
            {
                var obsList = new ObservableCollection<ConferencePaper>();
                foreach (var id in conferencePapers)
                {
                    obsList.Add((ConferencePaper)BookService.Instance.GetBook(id));
                }
                return obsList;
            }
            set
            {
                SetValue(ref _containedConferencePapers, value);
                conferencePapers.Clear();
                foreach (ConferencePaper conf in _containedConferencePapers)
                {
                    conferencePapers.Add(conf.Id);
                }
            }
        }

        public Array ReccurenceValues
        {
            get { return Enum.GetValues(typeof(Recurrence)); }
        }

        private Recurrence _recurrence;
        public Recurrence Recurrence
        {
            get
            {
                return _recurrence;
            }
            set
            {
                SetValue(ref _recurrence, value);
            }
        }

        private double _firstPublished;
        public double FirstPublished
        {
            get { return _firstPublished; }
            set { SetValue(ref _firstPublished, value); }
        }

        public DateTime FirstPublishedIn
        {
            get
            {
                return TimeStampToDateTime.UnixTimeStampToDateTime(FirstPublished);
            }
            set
            {
                FirstPublished = TimeStampToDateTime.DateTimeToUnixTimeStamp(value);
            }
        }
    }
}
