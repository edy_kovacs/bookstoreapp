﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using BookStoreApp.Models;
using BookStoreApp.Services;
using BookStoreApp.Utils.Constants;
using Newtonsoft.Json;
using Xamarin.Forms;
using System.Net.Http;
using System.Text;
using System.Linq;
using BookStoreApp.Views;
using BookStoreApp.Utils.Exceptions;

namespace BookStoreApp.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {

        #region Commands

        public ICommand LoginButtonCommand { get; private set; }
        public ICommand RegisterButtonCommand { get; private set; }
        public ICommand ChangeViewCommand { get; private set; }

        #endregion

        #region Properties

        private static readonly HttpClient _client = new HttpClient();

        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                SetValue(ref _email, value);
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                SetValue(ref _password, value);
            }
        }

        private string _confirmPassword;
        public string ConfirmPassword
        {
            get
            {
                return _confirmPassword;
            }
            set
            {
                SetValue(ref _confirmPassword, value);
            }
        }

        private bool _wasRegisterClicked;
        public bool WasRegisterClicked
        {
            get { return _wasRegisterClicked; }
            set { SetValue(ref _wasRegisterClicked, value); }
        }

        #endregion

        #region Constructor

        public LoginViewModel()
        {
            LoginButtonCommand = new Command(async () => await LoginHandlerAsync());
            RegisterButtonCommand = new Command(async () => await RegisterHandlerAsync());
            ChangeViewCommand = new Command(() => ChangeView());

            Email = AuthenticationManager.Instance.Username;
        }

        #endregion

        #region Private Methods

        private async Task LoginHandlerAsync()
        {
            IsBusy = true;
            var validationResult = ValidateCredentials();
            if (String.IsNullOrEmpty(validationResult))
            {
                try
                {
                    var credentials = new Credentials(Email, Password);
                    await AuthenticationService.Instance.LoginAsync(credentials);

                    var booksListPage = new NavigationPage(new BooksListPage())
                    {
                        BarBackgroundColor = CustomColors.ThemeBlue,
                        BarTextColor = Color.White,
                    };

                    var sideMenuPage = new MenuPage(new SideMenuViewModel());

                    var rootPage = new RootPage();
                    rootPage.Master = sideMenuPage;
                    rootPage.Detail = booksListPage;

                    IsBusy = false;
                    Application.Current.MainPage = rootPage;
                }
                catch (AuthenticationException exception)
                {
                    IsBusy = false;
                    await PageService.Instance.DisplayAlertAsync(Messages.ErrorMessage, exception.Message, Messages.OkMessageString);
                }
                catch (HttpRequestException)
                {
                    IsBusy = false;
                    await PageService.Instance.DisplayAlertAsync(Messages.PleaseFixMessage, Messages.CheckConnectionString, Messages.OkMessageString);
                }
            }
            else
            {
                IsBusy = false;
                await PageService.Instance.DisplayAlertAsync(Messages.ErrorMessage, validationResult, Messages.OkMessageString);
            }
        }

        private async Task RegisterHandlerAsync()
        {
            IsBusy = true;
            var validationResult = ValidateCredentials();
            if (String.IsNullOrEmpty(validationResult))
            {
                try
                {
                    var credentials = new Credentials(Email, Password);
                    await AuthenticationService.Instance.RegisterAsync(credentials);
                    IsBusy = false;
                    await PageService.Instance.DisplayAlertAsync(Messages.Success, Messages.SuccesfullyRegistered, Messages.BackToLogin);
                    ChangeView();
                }
                catch (AuthenticationException exception)
                {
                    IsBusy = false;
                    await PageService.Instance.DisplayAlertAsync(Messages.ErrorMessage, exception.Message, Messages.OkMessageString);
                }
                catch (HttpRequestException)
                {
                    IsBusy = false;
                    await PageService.Instance.DisplayAlertAsync(Messages.PleaseFixMessage, Messages.CheckConnectionString, Messages.OkMessageString);
                }
            }
            else
            {
                IsBusy = false;
                await PageService.Instance.DisplayAlertAsync(Messages.ErrorMessage, validationResult, Messages.OkMessageString);
            }
        }

        private string ValidateCredentials()
        {
            if (String.IsNullOrEmpty(Email) || String.IsNullOrEmpty(Password))
            {
                return Messages.IncorrectCompletionOfFields;
            }

            if (WasRegisterClicked == true)
            {
                if (String.IsNullOrEmpty(ConfirmPassword))
                {
                    return Messages.IncorrectCompletionOfFields;
                }

                if (Password.Length <= NumericConstants.MinimumPasswordLength)
                {
                    return Messages.PasswordTooShort;
                }

                if (!Password.Equals(ConfirmPassword))
                {
                    return Messages.PasswordsDontMatch;
                }
            }

            return null;
        }

        private void ChangeView()
        {
            Password = String.Empty;
            ConfirmPassword = String.Empty;
            WasRegisterClicked = !WasRegisterClicked;
        }

        #endregion

    }
}
