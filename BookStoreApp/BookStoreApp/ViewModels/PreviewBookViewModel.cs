﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using BookStoreApp.Models;
using BookStoreApp.Services;
using BookStoreApp.Views;
using Xamarin.Forms;

namespace BookStoreApp.ViewModels
{
    public class PreviewBookViewModel : BaseViewModel
    {
        public BookViewModel BookViewModel { get; private set; }

        public ICommand EditBookCommand { get; private set; }

        public PreviewBookViewModel(BookViewModel viewModel)
        {
            BookViewModel = viewModel;

            EditBookCommand = new Command(async () => await EditClicked());
        }

        public async Task EditClicked()
        {
            var viewModel = new BookDetailViewModel(BookViewModel);
            viewModel.BookUpdated += (source, updatedBook) =>
            {
                BookViewModel.Id = updatedBook.Id;
                BookViewModel.Title = updatedBook.Title;
                BookViewModel.Author = updatedBook.Author;
                BookViewModel.Genre = updatedBook.Genre;
                BookViewModel.PublishedDate = updatedBook.PublishedDate;
                BookViewModel.ImageURI = updatedBook.ImageURI;

                if (updatedBook is Journal updatedJournal)
                {
                    var journalViewModel = BookViewModel as JournalViewModel;
                    journalViewModel.FirstPublished = updatedJournal.FirstPublished;
                    journalViewModel.Recurrence = updatedJournal.Recurrence;
                    journalViewModel.conferencePapers = updatedJournal.ConferencePapers;
                }

                if (updatedBook is ConferencePaper updatedConferencePaper)
                {
                    var conferencePaperViewModel = BookViewModel as ConferencePaperViewModel;
                    conferencePaperViewModel.ConferenceLocation = updatedConferencePaper.ConferenceLocation;
                    conferencePaperViewModel.Abstract = updatedConferencePaper.Abstract;
                    conferencePaperViewModel.References = new ObservableCollection<string>(updatedConferencePaper.References);
                }
            };
            await PageService.Instance.PushAsync(new BookDetailPage(viewModel));
        }
    }
}
