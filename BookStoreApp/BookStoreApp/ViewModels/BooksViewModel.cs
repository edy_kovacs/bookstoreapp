﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using BookStoreApp.Models;
using BookStoreApp.Repositories;
using BookStoreApp.Services;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Utils.Converters;
using BookStoreApp.Utils.Exceptions;
using BookStoreApp.Views;
using Xamarin.Forms;

namespace BookStoreApp.ViewModels
{
    public class BooksViewModel : BaseViewModel
    {
        private BookViewModel _selectedBook;
        public ObservableCollection<BookViewModel> Books { get; private set; } = new ObservableCollection<BookViewModel>();

        // ShouldDisplayEmptyState means that books were loaded and the List of Books is empty after loading
        private bool _shouldDisplayEmptyState;
        public bool ShouldDisplayEmptyState
        {
            get
            {
                return _shouldDisplayEmptyState;
            }
            set
            {
                SetValue(ref _shouldDisplayEmptyState, value);
            }
        }

        private bool _isPageEnabled = true;
        public bool IsPageEnabled
        {
            get
            {
                return _isPageEnabled;
            }
            set
            {
                SetValue(ref _isPageEnabled, value);
            }
        }


        public ICommand LoadDataCommand { get; private set; }
        public ICommand DeleteBookCommand { get; private set; }
        public ICommand SelectBookCommand { get; private set; }
        public ICommand AddBookCommand { get; private set; }

        public BooksViewModel()
        {
            LoadDataCommand = new Command(async () => await LoadDataAsync());
            DeleteBookCommand = new Command<BookViewModel>(async vm => await DeleteBookAsync(vm));
            SelectBookCommand = new Command<BookViewModel>(async vm => await SelectBookAsync(vm));
            AddBookCommand = new Command(async () => await AddBookAsync());

            MessagingCenter.Subscribe<BookDetailViewModel, Book>(this, Messages.BookAddedEventString, AddBook);
            MessagingCenter.Subscribe<SideMenuViewModel>(this, Messages.LogoutStartedEventString, ChangeEnableStateOfPage);
            MessagingCenter.Subscribe<SideMenuViewModel>(this, Messages.LogoutEndedventString, ChangeEnableStateOfPage);

            ShouldDisplayEmptyState = true;
        }

        private async Task LoadDataAsync()
        {
            IsBusy = true;
            ShouldDisplayEmptyState = false;
            try
            {
                var books = await BookService.Instance.GetBooksAsync();

                foreach (var book in books)
                {
                    var viewModel = ViewModelToDomainModelConverter.ToViewModel(book);
                    Books.Add(viewModel);
                }
            }
            catch (RepositoryException e)
            {
                IsBusy = false;
                await PageService.Instance.DisplayAlertAsync(Messages.ErrorMessage, e.Message, Messages.OkMessageString);
            }
            ShouldDisplayEmptyState = Books.Count == 0;
            IsBusy = false;
        }

        public BookViewModel SelectedBook
        {
            get { return _selectedBook; }

            set { SetValue(ref _selectedBook, value); }
        }

        public async Task SelectBookAsync(BookViewModel book)
        {
            if(!IsBusy)
            {
                IsBusy = true;
                if (book == null)
                {
                    return;
                }

                SelectedBook = null;

                var viewModel = new PreviewBookViewModel(book);
                await PageService.Instance.PushAsync(new PreviewBookPage(viewModel));
                IsBusy = false;
            }
        }

        public async Task DeleteBookAsync(BookViewModel bookViewModel)
        {
            if (await PageService.Instance.DisplayAlertAsync(Messages.DeleteConfirmation, Messages.DeleteConfirmationDesc, Messages.DeleteMessageString, Messages.CancelMessageString))
            {
                if(!IsBusy)
                {
                    try
                    {
                        IsBusy = true;
                        await BookService.Instance.DeleteBookAsync(ViewModelToDomainModelConverter.ToDomainModel(bookViewModel));
                        ShouldDisplayEmptyState = true;
                        Books.Remove(bookViewModel);
                        IsBusy = false;
                    }
                    catch (UserException e)
                    {
                        IsBusy = false;
                        await PageService.Instance.DisplayAlertAsync(Messages.ErrorMessage, e.Message, Messages.OkMessageString);
                    }
                }
                else
                {
                    await PageService.Instance.DisplayAlertAsync(Messages.ErrorMessage, Messages.DeleteNotAllowed, Messages.OkMessageString);
                }
            }
        }

        public async Task AddBookAsync()
        {
            if(IsPageEnabled)
            {
                IsBusy = true;
                var viewModel = new ChooseTypeViewModel();
                await PageService.Instance.PushAsync(new ChooseTypePage(viewModel));
                IsBusy = false;
            }
            else
            {
                await PageService.Instance.DisplayAlertAsync(Messages.WarningMessage, Messages.WhileLoggingOut, Messages.OkMessageString);
            }
        }

        private void AddBook(BookDetailViewModel source, Book book)
        {
            Books.Add(ViewModelToDomainModelConverter.ToViewModel(book));
        }

        private void ChangeEnableStateOfPage(SideMenuViewModel source)
        {
            IsPageEnabled = !IsPageEnabled;
        }
    }
}