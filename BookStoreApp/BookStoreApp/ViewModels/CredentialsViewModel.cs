﻿using System;
namespace BookStoreApp.ViewModels
{
    public class CredentialsViewModel : BaseViewModel
    {
        public CredentialsViewModel()
        {
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetValue(ref _email, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetValue(ref _password, value); }
        }
    }
}
