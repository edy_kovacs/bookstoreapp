﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using BookStoreApp.Models;
using BookStoreApp.Services;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Utils.Converters;
using Xamarin.Forms;

namespace BookStoreApp.ViewModels
{
    public class BookViewModel : BaseViewModel
    {

        public BookViewModel()
        {
        }

        public BookViewModel(Book book)
        {
            Id = book.Id;
            Title = book.Title;
            Author = book.Author;
            Genre = book.Genre;
            PublishedDate = book.PublishedDate;
            ImageURI = book.ImageURI;
        }

        public string Id { get; set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }

        public bool IsImageURIEmpty
        {
            get
            {
                return ImageURI == null;
            }
        }

        private string _author;
        public string Author
        {
            get { return _author; }
            set { SetValue(ref _author, value); }
        }

        private string _genre;
        public string Genre
        {
            get { return _genre; }
            set { SetValue(ref _genre, value); }
        }

        private string _imageURI;
        public string ImageURI
        {
            get { return _imageURI; }
            set { SetValue(ref _imageURI, value); }
        }


        private double _publishedDate;
        public double PublishedDate
        {
            get { return _publishedDate; }
            set { SetValue(ref _publishedDate, value); }
        }

        public DateTime DateOfPublish
        {
            get
            {
                return TimeStampToDateTime.UnixTimeStampToDateTime(PublishedDate);
            }
            set
            {
                PublishedDate = TimeStampToDateTime.DateTimeToUnixTimeStamp(value);
            }
        }
    }
}