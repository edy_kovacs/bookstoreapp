﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using BookStoreApp.Models;
using BookStoreApp.Services;
using BookStoreApp.Utils.Converters;
using Xamarin.Forms;
using System.Linq;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Validators;
using BookStoreApp.Utils.Exceptions;
using System.Net.Http;
using System.Reflection;
using BookStoreApp.Utils.Extensions;

namespace BookStoreApp.ViewModels
{
    public class BookDetailViewModel : BaseViewModel
    {
        #region EventHandlers

        public event EventHandler<Book> BookUpdated;

        #endregion

        #region Properties

        public BookViewModel ViewModel { get; set; }
        private bool WasBookModified { get; set; }

        private ObservableCollection<Book> _allConferencePapers = new ObservableCollection<Book>();
        public ObservableCollection<Book> AllConferencePapers
        {
            get
            {
                return _allConferencePapers;
            }
            set
            {
                SetValue(ref _allConferencePapers, value);
            }
        }

        private bool _shouldDisplayPicker;
        public bool ShouldDisplayPicker
        {
            get
            {
                if (ViewModel.GetType().GetRuntimeProperty(Messages.RecurrenceString) != null && AllConferencePapers.Count > 0)
                {
                    _shouldDisplayPicker = true;
                }
                return _shouldDisplayPicker;
            }
            set
            {
                SetValue(ref _shouldDisplayPicker, value);
            }
        }

        private int _allocatedHeightForListviews;
        public int AllocatedHeightForListviews
        {
            get
            {
                return _allocatedHeightForListviews;
            }
            set
            {
                SetValue(ref _allocatedHeightForListviews, value);
            }
        }

        #endregion

        #region Commands

        public ICommand SaveCommand { get; private set; }
        public ICommand SelectedConferencePaperCommand { get; private set; }
        public ICommand SelectedRecurrenceCommand { get; private set; }
        public ICommand AbstractChangedCommand { get; private set; }
        public ICommand AddReferenceCommand { get; private set; }
        public ICommand WasBookModifiedCommand { get; private set; }
        public ICommand DeleteReferenceCommand { get; private set; }
        public ICommand DeleteContainedConferencePaperCommand { get; private set; }

        #endregion

        #region Constructor

        public BookDetailViewModel(BookViewModel viewModel)
        {
            SaveCommand = new Command(async () => await SaveAsync());
            SelectedConferencePaperCommand = new Command<ConferencePaper>(async (conferencePaper) => await AddContainedConferencePaperAsync(conferencePaper));
            SelectedRecurrenceCommand = new Command<Recurrence>(recurrence => UpdateRecurrence(recurrence));
            AbstractChangedCommand = new Command<string>(newAbstract => UpdateAbstract(newAbstract));
            AddReferenceCommand = new Command<string>(newReference => AddReference(newReference));
            DeleteReferenceCommand = new Command<string>(reference => DeleteReference(reference));
            DeleteContainedConferencePaperCommand = new Command<ConferencePaper>(conferencePaper => DeleteContainedConferencePaper(conferencePaper));
            WasBookModifiedCommand = new Command(() => WasBookModified = true);

            ViewModel = ViewModelToDomainModelConverter.CloneViewModel(viewModel);

            if(ViewModel is JournalViewModel journalViewModel)
            {
                UpdateAllConferencePapers(journalViewModel);
            }
            ChangeListHeight(ViewModel);
        }

        #endregion

        #region Private Methods

        async Task SaveAsync()
        {
            IsBusy = true;
            var domainModel = ViewModelToDomainModelConverter.ToDomainModel(ViewModel);
            if (String.IsNullOrEmpty(domainModel.Id))
            {
                try
                {
                    await BookService.Instance.AddBookAsync(domainModel);
                    MessagingCenter.Send(this, Messages.BookAddedEventString, domainModel);
                    await PageService.Instance.PopToRoot();
                    IsBusy = false;
                }
                catch (UserException e)
                {
                    IsBusy = false;
                    await PageService.Instance.DisplayAlertAsync(Messages.PleaseFixMessage, e.Message, Messages.OkMessageString);
                }
                catch (HttpRequestException)
                {
                    IsBusy = false;
                    await PageService.Instance.DisplayAlertAsync(Messages.PleaseFixMessage, Messages.CheckConnectionString, Messages.OkMessageString);
                }
            }
            else
            {
                if (WasBookModified)
                {
                    try
                    {
                        await BookService.Instance.UpdateBookAsync(domainModel);
                        BookUpdated?.Invoke(this, domainModel);
                        await PageService.Instance.PopAsync();
                        IsBusy = false;
                    }
                    catch (UserException e)
                    {
                        IsBusy = false;
                        await PageService.Instance.DisplayAlertAsync(Messages.PleaseFixMessage, e.Message, Messages.OkMessageString);
                    }
                    catch (HttpRequestException)
                    {
                        IsBusy = false;
                        await PageService.Instance.DisplayAlertAsync(Messages.PleaseFixMessage, Messages.CheckConnectionString, Messages.OkMessageString);
                    }
                }
                else
                {
                    IsBusy = true;
                    await PageService.Instance.PopToRoot();
                    IsBusy = false;
                }
            }
        }

        async Task AddContainedConferencePaperAsync(ConferencePaper conferencePaper)
        {
            var journalViewModel = ViewModel as JournalViewModel;
            if (journalViewModel.ContainedConferencePapers.Any(confPaper => confPaper.Title == conferencePaper.Title))
            {
                await PageService.Instance.DisplayAlertAsync(Messages.ErrorMessage, Messages.CannotAddConferencePaper, Messages.OkMessageString);
            }
            else
            {
                var alreadyContained = journalViewModel.ContainedConferencePapers;
                alreadyContained.Add(conferencePaper);
                journalViewModel.ContainedConferencePapers = alreadyContained;

                UpdateAllConferencePapers(journalViewModel);

                if (AllConferencePapers.Count == 0)
                {
                    ShouldDisplayPicker = false;
                }

                ChangeListHeight(journalViewModel);
            }
        }

        void UpdateRecurrence(Recurrence recurrence)
        {
            (ViewModel as JournalViewModel).Recurrence = recurrence;
            WasBookModified = true;
        }

        void UpdateAbstract(string newAbstract)
        {
            (ViewModel as ConferencePaperViewModel).Abstract = newAbstract;
            WasBookModified = true;
        }

        void AddReference(string newReference)
        {
            var cPaperViewModel = (ViewModel as ConferencePaperViewModel);
            cPaperViewModel.References.Add(newReference);
            WasBookModified = true;
            ChangeListHeight(cPaperViewModel);
        }

        void DeleteReference(string reference)
        {
            (ViewModel as ConferencePaperViewModel).References.Remove(reference);
            WasBookModified = true;
        }

        void DeleteContainedConferencePaper(ConferencePaper conferencePaper)
        {
            var journalViewModel = ViewModel as JournalViewModel;

            var alreadyContained = journalViewModel.ContainedConferencePapers;
            alreadyContained.Remove(conferencePaper);
            journalViewModel.ContainedConferencePapers = alreadyContained;

            WasBookModified = true;
            ShouldDisplayPicker = true;
            UpdateAllConferencePapers(journalViewModel);
            ChangeListHeight(journalViewModel);
        }

        private void UpdateAllConferencePapers(JournalViewModel journalViewModel)
        {
            var cpapersFromService = BookService.Instance.GetConferencePapers().ToList();

            foreach (var book in journalViewModel.ContainedConferencePapers)
            {
                cpapersFromService.Remove(book);
            }

            AllConferencePapers = new ObservableCollection<Book>(cpapersFromService);
        }

        private void ChangeListHeight(BookViewModel viewModel)
        {
            const int unitsForElement = 40;
            const int unitsForSpaceBetweenElements = 10;
            if(viewModel is ConferencePaperViewModel cPaperViewModel)
            {
                AllocatedHeightForListviews = cPaperViewModel.References.Count * unitsForElement + cPaperViewModel.References.Count * unitsForSpaceBetweenElements;
            }
            else if(viewModel is JournalViewModel journalViewModel)
            {
                AllocatedHeightForListviews = journalViewModel.ContainedConferencePapers.Count * unitsForElement + journalViewModel.ContainedConferencePapers.Count * unitsForSpaceBetweenElements;
            }
        }

        #endregion

    }
}
