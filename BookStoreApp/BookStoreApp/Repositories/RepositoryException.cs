﻿using System;
namespace BookStoreApp.Repositories
{
    public class RepositoryException : Exception
    {
        public RepositoryException(string message) : base(message) { }
    }
}
