﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BookStoreApp.Models;
using BookStoreApp.Services;
using BookStoreApp.Utils.Constants;
using BookStoreApp.Utils.Converters;
using BookStoreApp.Utils.Exceptions;
using BookStoreApp.Utils.Extensions;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace BookStoreApp.Repositories
{
    public class BookRestRepository
    {
        private static readonly HttpClient _httpClient = new HttpClient();
        Random rnd;

        public BookRestRepository()
        {
            rnd = new Random();
        }

        public async Task<IEnumerable<Book>> GetAll()
        {
            List<Book> booklist = new List<Book>();

            var AuthToken = AuthenticationManager.Instance.AuthToken;
            var url = new Uri(RESTStrings.PublishablesURL);
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, url);
            requestMessage.Headers.Add(RESTStrings.XAuthString, AuthToken);
            HttpResponseMessage response = await _httpClient.SendAsync(requestMessage);

            try
            {
                var books = JsonConvert.DeserializeObject<Dictionary<string, List<Book>>>(response.Content.ReadAsStringAsync().Result)[RESTStrings.BooksList];
                var conferencePapers = JsonConvert.DeserializeObject<Dictionary<string, List<ConferencePaper>>>(response.Content.ReadAsStringAsync().Result)[RESTStrings.ConferencePapersList];
                var journals = JsonConvert.DeserializeObject<Dictionary<string, List<Journal>>>(response.Content.ReadAsStringAsync().Result)[RESTStrings.JournalsList];

                foreach (var book in books)
                {
                    book.ImageURI = Messages.ImageBaseURL + rnd.Next(780, 790).ToString();
                    booklist.Add(book);
                }

                foreach (var conferencePaper in conferencePapers)
                {
                    conferencePaper.ImageURI = Messages.ImageBaseURL + rnd.Next(780, 790).ToString();
                    booklist.Add(conferencePaper);
                }

                foreach (var journal in journals)
                {
                    journal.ImageURI = Messages.ImageBaseURL + rnd.Next(780, 790).ToString();
                    booklist.Add(journal);
                }

                return booklist;
            }
            catch (KeyNotFoundException)
            {
                throw new RepositoryException(Messages.ErrorLoadingBooks);
            }
        }

        public async Task<string> SaveAsync(Book book)
        {
            var AuthToken = AuthenticationManager.Instance.AuthToken;
            var url = ModelToEndpointConverter.Convert(book);
            var content = new StringContent(JsonConvert.SerializeObject(book, RESTSettings.SerializerSettings), Encoding.UTF8, RESTStrings.JsonTypeOfContent);
            content.Headers.Add(RESTStrings.XAuthString, AuthToken);
            HttpResponseMessage response = await _httpClient.PostAsync(url, content);

            var whatever = response.StatusCode.ToString();

            if (response.IsSuccessStatusCode)
            {
                var bookAdded = JsonConvert.DeserializeObject<Book>(response.Content.ReadAsStringAsync().Result);
                return bookAdded.Id;
            }
            else if(response.StatusCode.ToString().Equals(RESTStrings.UnauthorizedErrorString))
            {
                throw new RepositoryException(Messages.SessionExpired);
            }
            else
            {
                throw new RepositoryException(Messages.ErrorAddingBook);
            }
        }

        public async Task DeleteAsync(Book book)
        {
            var AuthToken = AuthenticationManager.Instance.AuthToken;
            var url = ModelToEndpointConverter.ConvertToDeleteEndpoint(book); 
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Delete, url);
            requestMessage.Headers.Add(RESTStrings.XAuthString, AuthToken);
            HttpResponseMessage response = await _httpClient.SendAsync(requestMessage);

            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode.ToString().Equals(RESTStrings.UnauthorizedErrorString))
                {
                    throw new RepositoryException(Messages.SessionExpired);
                }
                else if (response.ReasonPhrase.Equals(RESTStrings.BadRequestErrorString))
                {
                    throw new RepositoryException(Messages.ErrorBookAlreadyDeleted);
                }
                else
                {
                    throw new RepositoryException(Messages.ErrorDeletingBook);
                }
            }
        }

        public async Task UpdateAsync(Book book)
        {
            var AuthToken = AuthenticationManager.Instance.AuthToken;
            var url = ModelToEndpointConverter.ConvertToDeleteEndpoint(book);
            var content = new StringContent(JsonConvert.SerializeObject(book, RESTSettings.SerializerSettings), Encoding.UTF8, RESTStrings.JsonTypeOfContent);
            content.Headers.Add(RESTStrings.XAuthString, AuthToken);
            HttpResponseMessage response = await _httpClient.PatchAsync(url, content);

            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode.ToString().Equals(RESTStrings.UnauthorizedErrorString))
                {
                    throw new RepositoryException(Messages.SessionExpired);
                }
                else
                {
                    throw new RepositoryException(Messages.ErrorUpdatingBook);
                }
            }
        }
    }
}
