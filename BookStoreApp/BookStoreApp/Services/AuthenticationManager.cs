﻿using System;
using BookStoreApp.Utils.Constants;
using Xamarin.Forms;

namespace BookStoreApp.Services
{
    public class AuthenticationManager
    {
        private static AuthenticationManager _instance;
        public string AuthToken { get; private set; }
        public string Username { get; private set; }

        private AuthenticationManager()
        {
            if (Application.Current.Properties.ContainsKey(RESTStrings.XAuthString) && Application.Current.Properties[RESTStrings.XAuthString] != null)
            {
                AuthToken = (string)Application.Current.Properties[RESTStrings.XAuthString];
            }
            if (Application.Current.Properties.ContainsKey(RESTStrings.EmailString) && Application.Current.Properties[RESTStrings.EmailString] != null)
            {
                Username = (string)Application.Current.Properties[RESTStrings.EmailString];
            }
        }

        public static AuthenticationManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AuthenticationManager();
                }
                return _instance;
            }
        }


        public bool IsLoggedIn()
        {
            if (Application.Current.Properties.ContainsKey(RESTStrings.XAuthString) && Application.Current.Properties[RESTStrings.XAuthString] != null)
            {
                return true;
            }

            return false;
        }

        public void LogIn(string Email, string AuthToken)
        {
            Application.Current.Properties[RESTStrings.EmailString] = Email;
            Application.Current.Properties[RESTStrings.XAuthString] = AuthToken;
            Application.Current.SavePropertiesAsync();
            this.AuthToken = AuthToken;
            this.Username = Email;
        }

        public void LogOut()
        {
            Application.Current.Properties.Clear();
        }
    }
}
