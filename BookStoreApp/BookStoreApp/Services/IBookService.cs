﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookStoreApp.Models;

namespace BookStoreApp.Services
{
    public interface IBookService
    {
        Task<IEnumerable<Book>> GetBooksAsync();
        IEnumerable<Book> GetConferencePapers();
        Book GetBook(string id);
        Task DeleteBookAsync(Book book);
        Task UpdateBookAsync(Book book);
        Task AddBookAsync(Book book);
    }
}
