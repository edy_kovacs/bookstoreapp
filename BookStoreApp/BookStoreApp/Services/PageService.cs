﻿using System;
using System.Threading.Tasks;
using BookStoreApp.Views;
using Xamarin.Forms;

namespace BookStoreApp.Services
{
    public class PageService : IPageService
    {
        private static PageService _instance = null;

        private PageService()
        {
        }

        public static PageService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PageService();
                }
                return _instance;
            }
        }

        public async Task<bool> DisplayAlertAsync(string title, string message, string ok, string cancel)
        {
            return await Application.Current.MainPage.DisplayAlert(title, message, ok, cancel);
        }

        public async Task DisplayAlertAsync(string title, string message, string ok)
        {
            await Application.Current.MainPage.DisplayAlert(title, message, ok);
        }

        public async Task PopAsync()
        {
            await ((MasterDetailPage)Application.Current.MainPage).Detail.Navigation.PopAsync();
        }

        public async Task PushAsync(Page page)
        {
            await ((MasterDetailPage)Application.Current.MainPage).Detail.Navigation.PushAsync(page);
        }

        public async Task PopToRoot()
        {
            await ((MasterDetailPage)Application.Current.MainPage).Detail.Navigation.PopToRootAsync();
        }
    }
}
