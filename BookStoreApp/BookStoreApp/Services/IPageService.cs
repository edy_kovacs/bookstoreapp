﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BookStoreApp.Services
{
    public interface IPageService
    {
        Task PushAsync(Page page);
        Task<bool> DisplayAlertAsync(string title, string message, string ok, string cancel);
        Task DisplayAlertAsync(string title, string message, string ok);
        Task PopAsync();
        Task PopToRoot();
    }
}
