﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BookStoreApp.Models;
using BookStoreApp.Repositories;
using BookStoreApp.Utils;
using BookStoreApp.Utils.Factories;
using BookStoreApp.Validators;

namespace BookStoreApp.Services
{
    public class BookService : IBookService
    {
        private List<Book> _bookList = new List<Book>();
        private static BookService _instance = null;
        private BookRestRepository _bookRepository = new BookRestRepository();

        private BookService()
        {

        }

        public static BookService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new BookService();
                }
                return _instance;
            }
        }

        public Book GetBook(string id)
        {
            try
            {
                return _bookList.Single(book => book.Id.Equals(id));
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public async Task<IEnumerable<Book>> GetBooksAsync()
        {
            _bookList = (await _bookRepository.GetAll()).ToList();
            return _bookList;
        }

        public IEnumerable<Book> GetConferencePapers()
        {
            return _bookList.Where(book => book is ConferencePaper);
        }

        public async Task DeleteBookAsync(Book book)
        {
            await _bookRepository.DeleteAsync(book);
            _bookList.RemoveAll((book2) => book2.Id.Equals(book.Id));
        }

        public async Task UpdateBookAsync(Book book)
        {
            await _bookRepository.UpdateAsync(book);
            _bookList[_bookList.FindIndex(b => b.Id.Equals(book.Id))] = book;
        }

        public async Task AddBookAsync(Book book)
        {
            BookValidator.Validate(book);
            book.Id = await _bookRepository.SaveAsync(book);
            book.ImageURI = ImageUriFactory.GetUri();
            _bookList.Add(book);
        }
    }
}
