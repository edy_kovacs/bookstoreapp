﻿using System;
using System.Net.Http;
using System.Text;
using BookStoreApp.Models;
using BookStoreApp.Utils.Constants;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using BookStoreApp.Utils.Exceptions;
using Xamarin.Forms;

namespace BookStoreApp.Services
{
    public class AuthenticationService
    {
        private static AuthenticationService _instance = null;
        private static readonly HttpClient _client = new HttpClient();
        public string AuthToken;

        private AuthenticationService()
        {
        }

        public static AuthenticationService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AuthenticationService();
                }
                return _instance;
            }
        }

        public async Task LoginAsync(Credentials credentials)
        {
            var url = new Uri(RESTStrings.AuthURL);
            var jsonContent = new StringContent(JsonConvert.SerializeObject(credentials).ToLower(), Encoding.UTF8, RESTStrings.JsonTypeOfContent);
            HttpResponseMessage response = await _client.PostAsync(url, jsonContent);

            if (response.IsSuccessStatusCode)
            {
                IEnumerable<string> headerValues;

                if (response.Headers.TryGetValues(RESTStrings.XAuthString, out headerValues))
                {
                    AuthToken = headerValues.FirstOrDefault();
                    AuthenticationManager.Instance.LogIn(credentials.Email, AuthToken);
                }
                else
                {
                    throw new AuthenticationException(Messages.AuthTokenNotGranted);
                }
            }
            else
            {
                throw new AuthenticationException(Messages.InvalidCredentials);
            }
        }

        public async Task RegisterAsync(Credentials credentials)
        {
            var url = new Uri(RESTStrings.SignUpURL);
            var jsonContent = new StringContent(JsonConvert.SerializeObject(credentials).ToLower(), Encoding.UTF8, RESTStrings.JsonTypeOfContent);
            HttpResponseMessage response = await _client.PostAsync(url, jsonContent);

            if (response.IsSuccessStatusCode)
            {
                IEnumerable<string> headerValues;
                if (response.Headers.TryGetValues(RESTStrings.XAuthString, out headerValues))
                {
                    AuthToken = headerValues.FirstOrDefault();
                }
                else
                {
                    throw new AuthenticationException(Messages.AuthTokenNotGranted);
                }
            }
            else
            {
                if ((int)response.StatusCode == 400)
                {
                    throw new AuthenticationException(Messages.DuplicateEmail);
                }
                else
                {
                    throw new AuthenticationException(Messages.UnexpectedError);
                }
            }
        }

        public async Task LogoutAsync()
        {
            AuthToken = AuthenticationManager.Instance.AuthToken;
            AuthenticationManager.Instance.LogOut();

            var url = new Uri(RESTStrings.SignOutURL);
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.TryAddWithoutValidation(RESTStrings.XAuthString, AuthToken);
            HttpResponseMessage response = await _client.DeleteAsync(url);

            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode.ToString().Equals(RESTStrings.UnauthorizedErrorString))
                {
                    throw new AuthenticationException(Messages.AlreadyLoggedOut);
                }
                else
                {
                    throw new AuthenticationException(Messages.UnexpectedError);
                }
            }
        }
    }
}
