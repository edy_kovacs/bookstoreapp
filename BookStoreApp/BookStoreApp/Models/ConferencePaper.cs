﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BookStoreApp.Models
{
    public class ConferencePaper : Book
    {
        public string FirstConferenceName { get; set; }
        public string Abstract { get; set; }
        public string ConferenceLocation { get; set; }
        public List<string> References { get; set; }

        public override string ToString()
        {
            return $"{Title}";
        }
    }
}
