﻿using System;

namespace BookStoreApp.Models
{
    public enum Recurrence
    {
        weekly,
        monthly,
        quarterly,
        yearly
    };
}
