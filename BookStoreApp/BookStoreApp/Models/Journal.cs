﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BookStoreApp.Utils.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BookStoreApp.Models
{
    public class Journal : Book
    {
        public double FirstPublished { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Recurrence Recurrence { get; set; }
        [JsonProperty(PropertyName = "conferencePapers")]
        public List<string> ConferencePapers { get; set; }
    }
}
