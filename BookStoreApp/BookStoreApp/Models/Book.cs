﻿using System;
using BookStoreApp.Utils.Converters;
using Newtonsoft.Json;

namespace BookStoreApp.Models
{
    public class Book : IHasID<string>
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Genre { get; set; }
        [JsonProperty(PropertyName = "publishedDate")]
        public double PublishedDate { get; set; }
        [JsonIgnore]
        public string ImageURI { get; set; }
    }
}
