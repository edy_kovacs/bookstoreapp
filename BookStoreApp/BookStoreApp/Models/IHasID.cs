﻿using System;

namespace BookStoreApp.Models
{
    public interface IHasID<T>
    {
        T Id { get; set; }
    }
}
