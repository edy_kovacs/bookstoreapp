﻿using BookStoreApp.Services;
using BookStoreApp.Utils.Constants;
using BookStoreApp.ViewModels;
using BookStoreApp.Views;
using Xamarin.Forms;

namespace BookStoreApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            if (AuthenticationManager.Instance.IsLoggedIn())
            {
                var booksListPage = new NavigationPage(new BooksListPage())
                {
                    BarBackgroundColor = CustomColors.ThemeBlue,
                    BarTextColor = Color.White,
                };

                var sideMenuPage = new MenuPage(new SideMenuViewModel());

                var rootPage = new RootPage();
                rootPage.Master = sideMenuPage;
                rootPage.Detail = booksListPage;

                Application.Current.MainPage = rootPage;
            }
            else
            {
                MainPage = new LoginPage();
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
