﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;

namespace BookStoreApp.Droid
{
    [Activity(Label = "BookStoreApp.Droid", Icon = "@drawable/applicationicon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
			Window.SetStatusBarColor(Android.Graphics.Color.Rgb(101, 149, 255));

            LoadApplication(new App());
        }
    }
}
