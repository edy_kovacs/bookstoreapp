﻿using System;
using Android.Runtime;
using Android.Views.InputMethods;
using Android.Widget;
using BookStoreApp.Droid.CustomRenderers;
using BookStoreApp.Utils.CustomElements;
using BookStoreApp.Utils.Enums;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace BookStoreApp.Droid.CustomRenderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            var element = this.Element as CustomEntry;

            if (element == null || this.Control == null)
            {
                Control.EditorAction -= (object sender, TextView.EditorActionEventArgs args) =>
                {
                    if (element.ReturnType != ReturnType.Next)
                        element.Unfocus();

                    element.InvokeCompleted();
                };

                return;
            }

            Control.SetBackgroundColor(global::Android.Graphics.Color.Transparent);

            if(element.HasBorder == false)
            {
                Control.Background = null;
            }

            SetReturnType(element);

            Control.EditorAction += (object sender, TextView.EditorActionEventArgs args) =>
            {
                if (element.ReturnType != ReturnType.Next)
                    element.Unfocus();

                element.InvokeCompleted();
            };
        }

        private void SetReturnType(CustomEntry entry)
        {
            ReturnType type = entry.ReturnType;

            switch (type)
            {
                case ReturnType.Go:
                    Control.ImeOptions = ImeAction.Go;
                    Control.SetImeActionLabel("Go", ImeAction.Go);
                    break;
                case ReturnType.Next:
                    Control.ImeOptions = ImeAction.Next;
                    Control.SetImeActionLabel("Next", ImeAction.Next);
                    break;
                case ReturnType.Send:
                    Control.ImeOptions = ImeAction.Send;
                    Control.SetImeActionLabel("Send", ImeAction.Send);
                    break;
                case ReturnType.Search:
                    Control.ImeOptions = ImeAction.Search;
                    Control.SetImeActionLabel("Search", ImeAction.Search);
                    break;
                default:
                    Control.ImeOptions = ImeAction.Done;
                    Control.SetImeActionLabel("Done", ImeAction.Done);
                    break;
            }
        }
    }
}
