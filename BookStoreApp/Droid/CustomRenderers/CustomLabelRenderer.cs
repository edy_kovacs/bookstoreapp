﻿using System;
using BookStoreApp.Droid.CustomRenderers;
using BookStoreApp.Utils.CustomElements;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRenderer))]
namespace BookStoreApp.Droid.CustomRenderers
{
    public class CustomLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            var element = this.Element as CustomLabel;

            if (element == null || this.Control == null)
            {
                return;
            }

            Control.SetSingleLine(false);
            Control.SetMaxLines(element.LinesNumber);
        }
    }
}
