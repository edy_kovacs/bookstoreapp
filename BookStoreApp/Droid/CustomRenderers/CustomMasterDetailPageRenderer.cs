﻿using System;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Widget;
using BookStoreApp.Droid.CustomRenderers;
using BookStoreApp.Utils.CustomElements;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android.AppCompat;

[assembly: ExportRenderer(typeof(CustomMasterDetailPage), typeof(CustomMasterDetailPageRenderer))]
namespace BookStoreApp.Droid.CustomRenderers
{
    public class CustomMasterDetailPageRenderer : MasterDetailPageRenderer
    {
        public CustomMasterDetailPageRenderer(Context context) : base(context) { }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            Android.Support.V7.Widget.Toolbar toolbar;
            if ((toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar)) != null)
            {
                for (int index = 0; index < toolbar.ChildCount; index++)
                {
                    if (toolbar.GetChildAt(index) is TextView)
                    {
                        var title = toolbar.GetChildAt(index) as TextView;
                        float toolbarCenter = toolbar.MeasuredWidth / 2;
                        float titleCenter = title.MeasuredWidth / 2;
                        title.SetX(toolbarCenter - titleCenter);
                    }
                }
            }
        }
    }
}
